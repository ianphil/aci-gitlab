#!/bin/bash

######################################################################
### Deploy GitLab as an Azure Container Instance using an ARM Template
### TDR - @tripdubroot
######################################################################

# Create Azure Resource Group
# az group create -n "GitLab" -l "eastus" --tags del=true

# Deploy tempate with existing Azure Storage Account
az group deployment create \
    --name "GitLabDeployment" \
    --resource-group "GitLab" \
    --template-file azuredeploy.json \
    --parameters \
        '{ \
            "storageAccountKey": {\
                "value": "mykey" \
            } \
        }'
